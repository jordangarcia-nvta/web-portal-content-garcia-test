<div class="row">
<div class="col-md-12">
<h3>Invitae&#39;s interpretation and reporting process: Step by step</h3>

<p></p>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="offset-panel">
<div class="number-color-willow2">1</div>

<h3>Variant research</h3>

<p>The first step of variant interpretation is to collect and research evidence related to each sequence change.</p>

<p>The following evidence types are included in this review:</p>

<ul class="bullet-list">
	<li>Previous observations of the variant in individuals</li>
	<li>The type of sequence change and the mechanism of disease</li>
	<li>Experimental evidence</li>
	<li>Indirect and predictive evidence</li>
	<li>Clinical interpretations from other laboratories (variant classifications from other laboratories are noted but are not considered to be independent pieces of evidence)</li>
</ul>

<p></p>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="offset-panel">
<div class="number-color-emerald">2</div>

<h3>Variant classification</h3>

<p>After the relevant information has been gathered and the variant has been thoroughly researched, a formal variant classification is assigned.</p>

<p>A formal classification is meant to help answer two questions about the clinical significance of the sequence change.&nbsp;</p>

<ul class="bullet-list">
	<li>From a <strong>diagnostic</strong> perspective: Does this sequence change, in the correct genetic background, provide an explanation for disease in an affected individual?</li>
	<li>From a <strong>predictive</strong> perspective, narrowly applied to highly penetrant conditions: Is an individual who inherits this sequence change likely to develop disease?</li>
</ul>

<p>The ACMG has recommended a five-tier classification system. According to this system, a sequence change can be classified as:</p>

<ul class="bullet-list">
	<li><strong>Pathogenic</strong>: This sequence change directly contributes to the development of disease. Some pathogenic sequence changes may not be fully penetrant. In the case of recessive or X-linked conditions, a single pathogenic sequence change may not be sufficient to cause disease on its own. Additional evidence is not expected to alter the classification of this sequence change. </li>
	<li><strong>Likely pathogenic</strong>: This sequence change is very likely to contribute to the development of disease; however, the scientific evidence is currently insufficient to prove this conclusively. Additional evidence is expected to confirm this assertion of pathogenicity, but we cannot fully rule out the possibility that new evidence may demonstrate that this sequence change has little or no clinical significance.</li>
	<li><strong>Uncertain significance</strong>: There is not enough information at this time to support a more definitive classification of this sequence change.</li>
	<li><strong>Likely benign</strong>: This sequence change is not expected to have a major effect on disease; however, the scientific evidence is currently insufficient to prove this conclusively. Additional evidence is expected to confirm this assertion, but we cannot fully rule out the possibility that new evidence may demonstrate that this sequence change can contribute to disease.</li>
	<li><strong>Benign</strong>: This variant does not cause disease.</li>
</ul>

<p>Two additional classifications are also periodically invoked to capture variable expressivity:</p>

<ul class="bullet-list">
	<li><strong>Pathogenic (low penetrance)</strong>: This variant is commonly accepted as a contributing factor of disease, but the penetrance of this particular change is sufficiently low (&lt;25%) to be often seen in individuals without disease. As a result, the predictive value of this information is considered to be low.</li>
	<li>
	<p><strong>Pseudodeficiency allele</strong>: This&nbsp;variant&nbsp;can lead to false positive results on biochemical enzyme studies, but is not known to cause clinical symptoms or lead to disease. Enzyme studies cannot differentiate between true pathogenic variants and pseudodeficiency alleles, so these must be distinguished by molecular studies.</p>
	</li>
</ul>

<p>Variants are assigned a formal classification based on:</p>

<ul class="bullet-list">
	<li>Weighted evidence: The evidence gathered during variant review is scored based on the relative importance of the evidence type. Certain types of evidence may support a pathogenic classification while others may support a benign classification.<br />
	&nbsp;</li>
	<li>Grouping by basic argument: If multiple pieces of evidence point to the same basic argument, only the strongest piece of evidence is considered.
	<ul class="bullet-list">
		<li>Example: A functional study that demonstrates reduced protein levels and an in silico algorithm that predicts that the variant is deleterious, both contribute to the general argument that &ldquo;protein function may be disrupted.&rdquo;</li>
	</ul>
	</li>
	<li>Additive vs. non-additive evidence: Certain types of evidence can be considered more than once, and certain types cannot.
	<ul class="bullet-list">
		<li>Example: Independent observations of the variant in multiple affected individuals are additive.</li>
		<li>Example: Multiple experimental studies demonstrating that different aspects of protein function are disrupted are non-additive.</li>
	</ul>
	</li>
	<li>Extensive review to ensure accurate and consistent classification: Each variant classification undergoes extensive review by our clinical reporting team. Invitae has developed proprietary software tools to efficiently record and share information, an important step to ensure reproducibility.</li>
</ul>

<p></p>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="offset-panel">
<div class="number-color-mountain">3</div>

<h3>Reporting</h3>

<p>Our clinical report:</p>

<ul class="bullet-list">
	<li>Highlights the most important findings for your patient</li>
	<li>Provides relevant clinical information about the diseases associated with the genes in which variants were identified</li>
	<li>Describes the relevance of the findings for your patient and their family</li>
	<li>Suggests possibilities for follow-up testing</li>
	<li>Describes in detail the evidence and logic supporting each variant interpretation</li>
	<li>Provides recurrence risk information when possible</li>
</ul>

<p>The report includes:</p>

<ul class="bullet-list">
	<li><strong>Summary</strong>: A clear statement summarizing the high-level result of the genetic test &mdash; positive, negative, or clinically inconclusive</li>
	<li><strong>Clinical summary</strong>: A description of the relevance of the genetic results to the patient based on their clinical and family history</li>
	<li><strong>Complete results table</strong>: A table of the genes in which genetic variants were identified, and a complete list of all genes analyzed in the test<sup>*</sup></li>
	<li><strong>Variant details</strong>: A summary of the evidence and logic used to justify the interpretation of each variant</li>
	<li><strong>Methods and limitations</strong>: A description of Invitae&#39;s next-generation sequencing assay along with assay limitations</li>
</ul>

<p>The field of genetics is constantly evolving, often revealing new evidence relevant to variant interpretation. When new evidence on a variant becomes available, we review our variant interpretation and, if indicated, we will reclassify the variant and issue an amended report to the ordering clinician. If a report is amended, the ordering clinician will receive a notification via phone or email.</p>

<p>Genetic testing can have health implications not only for an individual, but for an entire family. Invitae offers family variant testing at no additional charge for all first-degree relatives of patients who undergo gene or panel testing for a hereditary condition at Invitae and are found to have a pathogenic or likely pathogenic variant. To help resolve variants of uncertain significance (VUS) in our test results, Invitae also offers complimentary follow-up testing to select family members of patients tested at Invitae when informative data can be obtained. </p>

<p>To learn more about these programs, please visit our <a href="http://www.invitae.com/vus-resolution">VUS resolution</a> and <a href="https://www.invitae.com/en/family-testing/">family variant testing</a> webpages.</p>

<p class="endnote"><sup>*</sup>Benign variants and silent and intronic variants with no evidence towards pathogenicity are not included in the report but are available upon request.</p>
</div>
</div>
</div>