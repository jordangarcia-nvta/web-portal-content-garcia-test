<div class="row">
<div class="col-md-12">
<h3>Frequently asked questions</h3>

<div class="faq-block">
<div class="faq-block__heading theme-one">Genetic testing with Invitae</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What is genetic testing?</h4>

<div class="expandable-content">
<p>Genetic testing analyzes your genes, which are the instructions encoded in your DNA. Your genes help determine your hair and eye color, height, and other physical traits that make you who you are. Genetic testing looks for variations in your genes that can potentially lead to disease. You or your doctor might consider genetic testing for a variety of reasons, such as confirming a disease you are suspected to have or understanding the cause of a disease that runs in your family.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What are some different types of genetic testing?</h4>

<div class="expandable-content">
<p>Invitae offers both diagnostic and predictive testing. <i>Diagnostic testing</i> is performed to genetically confirm a disease that your doctor suspects you might have, based on your symptoms. <i>Predictive testing</i> involves performing genetic testing to determine your future risk of developing a disease or condition. If you have a family history of a specific disease that may be inherited&mdash;whether it&rsquo;s a rare inherited illness or a more common one, like cancer&mdash;your doctor may recommend predictive testing to clarify your risk of developing the disease in the future.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What types of genetic testing does Invitae offer?</h4>

<div class="expandable-content">
<p>Invitae specializes in genetic testing for inherited conditions related to a variety of medical disciplines, including cardiology, hereditary cancers, pediatric genetics, metabolic disorders, neurology, and hematology. For more details on our genetic testing, visit our <a href="{% page_url 'portal_common.test-catalog' %}"> Test Catalog</a> page.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What support does Invitae offer during the testing process?</h4>

<div class="expandable-content">
<p>We understand that making choices about genetic testing can sometimes be overwhelming. Invitae offers support before, during, and after testing. We put all of our resources to work for you so that you can focus on caring for yourself, your family, and your future.</p>

<p>Invitae&rsquo;s genetic testing services include:</p>

<ul class="bullet-list">
	<li>High-quality, affordable genetic testing&mdash;including, in some cases, patient financial assistance programs</li>
	<li>Testing that&rsquo;s simple, convenient, and affordable</li>
	<li>Results in 10&ndash;21 calendar days (14 days on average)</li>
	<li>Expert analysis for clear understanding</li>
	<li>If appropriate, family testing to clarify risk for your relatives</li>
	<li>Help with insurance and the billing process in the US</li>
	<li>Convenient online access and the ability to track your sample throughout the testing process</li>
</ul>

<p>In addition, Invitae offers genetic counseling to assist you in understanding your genetic test results.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How do I know what type of genetic test is right for me?</h4>

<div class="expandable-content">
<p>Finding the right genetic test depends on your medical and family histories, your current medical conditions, and the kinds of answers you and your healthcare provider seek. Talk to your doctor about what type of genetic testing may be right for you, and&nbsp;<a href="{% page_url 'patient.contact' %}">contact</a> our Client Services team to learn more about your options.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What if I am found to have a genetic variation and my family members want to get tested?</h4>

<div class="expandable-content">
<p>Your genetic test results may have health implications not only for you, but for your family as well.</p>

<p>If you receive a positive genetic test result from Invitae, family variant testing can be used to identify other family members with the same variant and provide information about their genetic risks. To learn more about test options for your family, please talk with your doctor or <a href="{% page_url 'patient.contact' %}">contact</a> our Client Services team.</p>

<p>Invitae&rsquo;s family variant testing involves full analysis of the gene in which the original family member&rsquo;s variant was identified. The report will include the status of the familial variant as well as any other disease-causing or likely disease-causing variant(s) identified in that gene.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Who is eligible for family&nbsp;testing?</h4>

<div class="expandable-content">
<p>A family member of the original patient is eligible if all of the following criteria are met:</p>

<ul class="bullet-list">
	<li>The original patient was tested at Invitae*</li>
	<li>A disease-causing or likely disease-causing variant was identified in the original patient&#39;s test report**</li>
	<li>The family member is at risk for carrying the same variant</li>
</ul>

<p>*Testing at the family variant price is not available to families if the original patient&rsquo;s pathogenic variant was detected by a lab other than Invitae.</p>

<p>**If the original patient&#39;s test report identified a variant of uncertain significance (VUS), VUS resolution may be available at no additional charge to eligible family members. Family members of patients who don&rsquo;t qualify for VUS resolution at no additional charge may be able to take advantage of family variant testing.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What if my genetic test comes back negative or inconclusive?</h4>

<div class="expandable-content">
<p>If you receive a negative or inconclusive result on an Invitae test, your doctor may choose to test more genes within the original clinical area for which you underwent testing. These additional genes can be requested for no additional charge within 90 days of when your doctor received the first test result. (Only genes available at the time of the original order are eligible.) More information about this program is available on our <a href="{% page_url 'portal_common.re-requisition' %}">Re-requisition</a> page.</p>
</div>
</div>
</div>

<div class="faq-block">
<div class="faq-block__heading theme-two">The genetic testing process at Invitae</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How does it work?</h4>

<div class="expandable-content">
<p>Invitae&#39;s clinical genetic tests must be ordered by a certified, authorized healthcare professional.</p>

<p>We want to make the genetic testing process as smooth as possible. Once you and your physician decide that genetic testing is right for you, a blood or saliva sample can be submitted to Invitae for testing.</p>

<p>If you and your doctor choose to submit a blood sample, you can have your blood drawn at your physician&rsquo;s office or laboratory. If you require assistance with having your blood drawn, Invitae offers a convenient service to have it drawn in your home at no additional charge. This sample collection service is offered throughout the US and Canada.</p>

<p>Upon receipt of your sample, a team of scientists, clinicians, and genetic counselors conducts the genetic test and works together to review the results and provide expert interpretation. Your genetic test results are then sent to your doctor.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How do I provide a DNA sample?</h4>

<div class="expandable-content">
<p>Once you decide to proceed with testing and your doctor has ordered your test, you will need to provide either a blood sample or a saliva sample.</p>

<p>If you choose to submit a saliva sample, your doctor can request that Invitae ship a saliva sample collection kit to your doctor&rsquo;s office or to your home.</p>

<p>If you are in the US or Canada and choose to submit a blood sample, this can be collected at your doctor&rsquo;s office or in your own home&mdash;whichever you prefer.&nbsp;To request a blood draw service within the US or Canada, <a href="{% page_url 'patient.contact' %}">contact</a> our Client Services team. Appointments must be scheduled at least three days in advance. Invitae will ship a collection kit to your home or your doctor&rsquo;s office with all the necessary supplies and paperwork. This service is offered at no additional charge.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How long does it take to get my results?</h4>

<div class="expandable-content">
<p>We understand that patients would like to receive their genetic test results as soon as possible. With Invitae, your doctor will receive results in 10&ndash;21 calendar days (14 days on average). Our online portal makes it easy for you to know exactly where your sample is in the testing process. You can also see your results online once your doctor has reviewed and released them. The Invitae online portal can be found on our <a href="{% url 'patient.signin' %}">Sign In</a> page.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How do I check the status of my test?</h4>

<div class="expandable-content">
<p>After your doctor has ordered a test for you with Invitae, you can sign in to our online portal to check the status of your test. You can also view your test results once your doctor releases them. The Invitae online portal can be found on our <a href="{% url 'patient.signin' %}"> Sign In</a> page.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How do I log in to my Invitae account?</h4>

<div class="expandable-content">
<p>If your doctor entered your email address when he or she placed your test order, then an email was sent to you with the information you need to establish your Invitae online portal account. Your username for this account is your email address; you can set your password at the time you create your account. The online portal can be found on our <a href="{% url 'patient.signin' %}"> Sign In</a> page.</p>

<p>If you have lost your password or failed to receive the account setup email, please <a href="{% page_url 'patient.contact' %}">contact</a> our Client Services team.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What if my doctor doesn&rsquo;t know about Invitae?</h4>

<div class="expandable-content">
<p>Invitae&#39;s genetic tests must be ordered by a certified healthcare professional. If you are interested in learning about our genetic testing services, our Client Services team would be happy to assist you by:</p>

<ul class="bullet-list">
	<li>Providing more information about the tests we offer</li>
	<li>Helping to facilitate a conversation with your doctor</li>
	<li>Connecting you with one of our genetic counselors, who can answer your questions about the overall genetic testing process</li>
</ul>

<p>To learn more, please <a href="{% page_url 'patient.contact' %}">contact</a> our Client Services team. In addition, Invitae can help you identify a qualified genetics professional in your area through the Invitae Genetics Provider Network (GPN). Please <a href="{% url 'patient.signin' %}">sign in</a> to your Invitae account&mdash;or, if you don&#39;t yet have one, create an account&mdash;to connect with our network of genetic counselors, geneticists, and genetic nurses.</p>
</div>
</div>
</div>

<div class="faq-block">
<div class="faq-block__heading theme-three">Genetic counseling support through Invitae</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What is a genetic counselor and why does my doctor want me to speak with one?</h4>

<div class="expandable-content">
<p>Genetic counselors are specially trained healthcare providers who can assist you in understanding your genetic test result and what it means for you and for your family members.</p>

<p>Many physicians refer their patients to genetic counselors in order to provide them with the opportunity to learn more about genetic testing and to give them the chance to ask questions about the genetic testing process. When a genetic variant is identified, many physicians also appreciate the detailed manner in which genetic counselors help a patient to identify at-risk family members and provide the patient with resources about genetic testing that they can share with their relatives.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Does Invitae offer genetic counseling services?</h4>

<div class="expandable-content">
<p>If you do not have access to a local genetic counselor, Invitae offers remote genetic counseling services by telephone or video. (In the US, these services are performed in accordance with state requirements governing the practice of tele-genetics.) Currently, genetic counseling is available for cancer and cardiology genetic testing. Physician referral is required for Invitae&rsquo;s genetic counseling services. To learn more, please <a href="{% page_url 'patient.contact' %}">contact</a> our Client Services team.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Are Invitae&rsquo;s genetic counselors board-certified and licensed in the US?</h4>

<div class="expandable-content">
<p>Yes, Invitae employs genetic counselors certified by the American Board of Genetic Counseling. Invitae also employs genetic counselors licensed in every US state with licensure requirements.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How do I request a genetic counseling appointment?</h4>

<div class="expandable-content">
<p>Physician referral is required for Invitae&rsquo;s patient genetic counseling services. To learn more about how to connect with one of our genetic counselors, please <a href="{% page_url 'patient.contact' %}">contact</a> our Client Services team.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What should I expect during my genetic counseling appointment?</h4>

<div class="expandable-content">
<p>During your counseling session, your genetic counselor will briefly review your medical history and ask you questions about your family medical history. Using this information, your genetic counselor will explain the genetic test that your doctor has ordered and discuss possible test results, including what those results may mean both for you and for your family members. Your genetic counselor will also explain how genetic testing is performed, assist you with submitting your DNA specimen (should you choose to proceed with testing), and review your genetic test results with you when they are complete.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How can I prepare for my genetic counseling session?</h4>

<div class="expandable-content">
<p>Genetic counseling sessions are similar to other medical appointments. You should set aside one hour for your session and ensure that you are free from interruptions during this time.</p>

<p>Prior to your genetic counseling session, you may wish to ask your family members about your family medical history. It is most helpful if you ask them about any diagnosed medical conditions (such as cancer or heart disease) and the age at which each medical condition was diagnosed. You may also wish to ask them about the cause and age of death for deceased family members.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What if I am adopted or do not know the details of my family medical history?</h4>

<div class="expandable-content">
<p>It is not uncommon for individuals to be adopted or to know very little medical information about a particular side of the family. Rest assured that your genetic counselor will work with whatever information you have available to you during the genetic testing process.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How long does a genetic counseling session take?</h4>

<div class="expandable-content">
<p>Genetic counseling sessions typically last one hour, but they may take more or less time depending on the number of questions you have for the genetic counselor.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What if I have questions about my medical care?</h4>

<div class="expandable-content">
<p>Any questions related to your medical care should be directed to your managing physician.</p>
</div>
</div>
</div>

<div class="faq-block">
<div class="faq-block__heading theme-four">Cost and insurance coverage</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What does Invitae&rsquo;s genetic testing cost?</h4>

<div class="expandable-content">
<p>Invitae offers genetic analysis to identify the cause of a disease affecting an individual or a family. Our pricing is per clinical area, referring to disease areas such as oncology, or cardiology.</p>

<p>In rare cases, a family may be affected by more than one genetic condition, and in these cases it is possible to order tests for genes or panels relevant to more than one clinical area. In such a case, please <a href="{% page_url 'patient.contact' %}">contact</a> our Client Services team for more information.</p>

<p>In the US, our pricing per clinical area is:</p>

<ul class="bullet-list">
	<li><strong>$1,500:</strong> List price

	<ul class="bullet-list">
		<li>Invitae will never charge more than this for a panel test within a single clinical area<strong>.</strong></li>
		<li>To help provide accurate information regarding out-of-pocket insurance requirements, Invitae conducts a benefits investigation. Where Invitae&#39;s benefits investigation discloses a plan that requires a patient out of pocket in excess of $100, Invitae will notify the patient before incurring any costs.
		<ul class="bullet-list">
			<li>For more information, please <a href="{% page_url 'portal_common.contact' %}">contact</a> Client Services.</li>
		</ul>
		</li>
	</ul>
	</li>
	<li><strong>$475:</strong> Patient-pay price
	<ul class="bullet-list">
		<li>Available as a prepaid option only. Invitae will not submit to insurance or offer advice regarding insurance submission decisions.</li>
		<li>Patient agrees to register online and sets contact preferences.</li>
		<li>Patient assistance programs are not available for patient prepay option.</li>
		<li>Patient prepay orders must be placed online, including the requisition from the clinician.</li>
		<li>For international orders, the patient prepay option <strong>does not</strong> include shipping.</li>
	</ul>
	</li>
</ul>

<p>It is important to note that for patient-pay pricing, we require patients to go through their healthcare provider to order our tests. Healthcare professionals are fundamental to ordering the right test for the right patient and in interpreting genetic information.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Is Invitae&rsquo;s genetic testing covered by insurance in the US?</h4>

<div class="expandable-content">
<p>Invitae&#39;s genetic testing may be covered by your health insurance. You won&rsquo;t need to contact your insurance company to find out if testing is covered or to obtain reimbursement. Invitae will contact your insurance company directly to coordinate coverage and payment. You may receive an explanation of benefits (EOB) from your insurance company in the mail. This is not a bill. Invitae will also receive this EOB and will handle any appeals processes for you.</p>

<p>Medicare insurance may cover the cost of an Invitae test when specific criteria are met. Medicare only covers diagnostic testing, not testing based on family history alone or screening. If you are ordering a test that includes BRCA1 or BRCA2, you must meet BRCA testing criteria and have not previously had BRCA testing to qualify for coverage.</p>

<p>Even if your insurance plan doesn&rsquo;t cover testing, Invitae may be able to offer testing at limited or no expense to those who qualify for need-based financial assistance. Please <a href="{% page_url 'patient.contact' %}">contact</a> our Client Services team for more information.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Is Invitae&rsquo;s genetic testing covered by insurance outside of the US?</h4>

<div class="expandable-content">
<p>Invitae&#39;s genetic testing may be covered by your non-US health insurance. Contact your local insurance provider to ask if you are eligible for coverage and, if so, how to apply for it. If you are not eligible, contact your healthcare provider to see if a patient-pay option is right for you.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What payment options are available?</h4>

<div class="expandable-content">
<p>We offer the following billing options. To learn more about each of these choices, please visit our <a href="{% page_url 'portal_common.billing' %}">billing page</a>.</p>

<ul class="bullet-list">
	<li><strong>Patient pay</strong> &mdash; The patient pays in full upfront via credit card (we accept Visa, Mastercard, American Express, and JCB). Payment can also be submitted by wire transfer.</li>
	<li><strong>Institutional billing</strong> &mdash; Invitae will work with your institution to set up a contract if one is not already in place.</li>
	<li><strong>Third-party insurance (US)</strong> &mdash; Invitae will bill insurance carriers on your behalf. With Invitae, regardless of whether a patient is in-network or out-of-network, a patient will never be surprised by an out-of-pocket balance. To help provide patients with accurate information regarding out-of-pocket insurance requirements, Invitae conducts a benefits investigation. Where Invitae&#39;s benefits investigation discloses a plan that requires a patient out of pocket in excess of $100, Invitae will notify the patient before incurring any costs. Invitae has a Patient Assistance Plan that may apply in these situations. If a patient does not qualify for that plan they may still elect the upfront patient-pay price of $475, or opt to cancel the test and avoid all charges.</li>
	<li><strong>Medicare(US)</strong> &mdash; Medicare coverage is available when specific criteria are met.</li>
</ul>

<p>For those who qualify, we have a Patient Access Program (PAP) to reduce patient out-of-pocket costs. Please&nbsp;<a href="{% page_url 'patient.contact' %}">contact</a> our Client Services team for further information.</p>
</div>
</div>
</div>
</div>
</div>
