<p></p>

<h4>Patient Insights Networks help researchers and biopharmaceutical companies find better treatments, faster. Patients can contribute data about their experience with a disease and receive information about the latest research and clinical trial opportunities.</h4>

<div class="row">
<div class="col-md-12">
<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What is a Patient Insights Network?</h4>

<div class="expandable-content">
<p>A Patient Insights Network (PIN) is much more than a traditional registry. It’s an online, interactive platform for collecting, curating, coordinating and delivering patient data. You access it using a standard browser on a computer or mobile device. It’s HIPPA and FISMA compliant, secure and research-ready.</p>

<p>Invitae’s PIN network, which began in 2007 as PatientCrossroads, is made up of individual PINs established for more than 400 specific medical conditions. Our team has worked with the National Institutes of Health (NIH), the Patient-Centered Outcomes Research Institute (PCORI), numerous biopharma companies, and scores of advocacy organizations, and that expertise shapes our approach. Storing patient data in one place benefits patients, researchers and biopharmaceutical companies working to help find new and better treatments for disease. You can access our existing network or let us build you a customized PIN, in any language. We can easily expand or reconfigure PIN functionality to match any stage of biopharmaceutical development.</p>

<p>A PIN lets you build global, engaged patient communities and conduct customized surveys. Via a PIN, you can pre-qualify patients to streamline trial recruitment. You can customize notifications and content to improve engagement and adherence. You can also capture clinical data electronically and deliver compliant educational materials. Our PINs make it easy for patients to share their experience, contribute medical data and be connected to research and clinical trial opportunities.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What types of data can be collected?</h4>

<div class="expandable-content">
<p>Patients create accounts, complete demographic information and consent to participate. Then they can:</p>

<ul class="bullet-list">
	<li>
	<p>complete one or more health questionnaires</p>
	</li>
	<li>
	<p>identify their healthcare providers</p>
	</li>
	<li>
	<p>connect their healthcare system patient portal</p>
	</li>
	<li>
	<p>track medication usage</p>
	</li>
	<li>
	<p>track meaningful lab values</p>
	</li>
	<li>
	<p>upload relevant medical reports and important results</p>
	</li>
</ul>

<p>We can also develop targeted studies to meet your research needs.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How can the data quality be verified?</h4>

<div class="expandable-content">
<p>Invitae can provide data curation services by medical professionals who ensure patient provided data is accurate and complete.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Can a PIN import data from Electronic Medical Records?</h4>

<div class="expandable-content">
<p>Yes, PINs can accept EMR data from data files in HL7 format as long as the patient has access to these files through their healthcare provider portal. Past history, as well as on-going medical records can be uploaded to the PIN.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Can clinicians enter patient data?</h4>

<div class="expandable-content">
<p>Yes, clinical centers or research sites can enter data for patients associated with their organization. For studies that do not have a pre-determined set of sites, a participant's clinician can augment patient-provided data by completing provider questionnaires to enter relevant history and medical findings.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Can multiple diseases be represented in a single PIN?</h4>

<div class="expandable-content">
<p>Yes. PINs can support multiple diseases, as well as multiple outreach partners for the same disease. PIN access can be granted on an organization and disease basis. This enables large organizations to collect and aggregate PIN data to facilitate pan-disease analysis.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Can the data be collected under an institutional review board (IRB)?</h4>

<div class="expandable-content">
<p>Yes, PINs can support one or more consent processes for individual studies or sub-studies. Often a central IRB-like Chesapeake IRB is used for review of protocol and consent materials.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How is a Patient Insights Network marketed?</h4>

<div class="expandable-content">
<p>Patients can learn about a PIN via advocacy outreach, social media, internet searches, healthcare providers, or even testing labs.</p>

<p>Invitae works with each client to identify the outreach partners and recommend marketing strategies.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>How can patients be recruited into clinical trials and studies?</h4>

<div class="expandable-content">
<p>PIN data can be used to help screen participants for potential trial and study eligibility based on specific inclusion and exclusion criteria. Invitae facilitates trial recruitment in a manner that protects participant privacy. We send study notices to qualified participants and connect them to the relevant study information. Participants remain anonymous until they request contact from a study coordinator.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Can data be collected worldwide?</h4>

<div class="expandable-content">
<p>Yes. Our Patient Insights Networks have been used by many international partners. There are multiple models for recruitment. A single global program can be established where all partners refer to the same PIN, or an umbrella model can be used so that each country has a customizable portal into the Patient Insights Network.  Google Translate can be provided on the site, or the site content can be manually translated.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Who owns the data?</h4>

<div class="expandable-content">
<p>Patients own their data. Patients opt in and choose to share their information. They can just as easily opt out. We are grateful that so many patients are willing to share their de-identified information to support the development of new treatments.</p>

<p>Invitae acts as the trusted third party gatekeeper of information, gathering data and ensuring it is safeguarded and shared appropriately. Due to competitive or regulatory requirements, there are situations where biopharmaceutical companies or researchers collect data that they want to keep private. However, we encourage all PINs to share data broadly to further research and charge a fee for keeping data private.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Is the data secure?</h4>

<div class="expandable-content">
<p>Protecting patient data is Invitae’s priority. Invitae operates all systems including the Patient Insights Networks in rigorous compliance with HIPAA and FISMA. Learn more on our <a href="http://www.invitae.com/PIN-security">PIN security and privacy webpage</a>.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>What is Invitae’s role in communicating with patients?</h4>

<div class="expandable-content">
<p>Invitae serves as a trusted intermediary. We can distribute emails with program branding to patients who have expressed their willingness to receive information. Messages may target participants who meet specific criteria or be sent to all participants in a PIN.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Does Invitae share participants' contact information?</h4>

<div class="expandable-content">
<p>Invitae protects participant privacy. We only share contact information if a participant requests that it be shared. We distribute all messages to participants through the Patient Insights Network, in accordance with the participant's contact and communication preferences.</p>
</div>
</div>
</div>
</div>