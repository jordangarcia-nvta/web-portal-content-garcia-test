<div class="row">
<div class="col-md-12">
<h3>Genetic test results you can trust</h3>

<p>To demonstrate that Invitae's next-generation sequencing (NGS) analysis provides the high-quality results you are accustomed to, Invitae has validated our analytic results and clinical interpretations.</p>

<p><strong>Updates</strong></p>

<ul class="bullet-list">
	<li>View the Invitae hereditary cancer validation study to learn more about our recently published study in the <i>Journal of Molecular Diagnostics</i> in collaboration with the Stanford University School of Medicine and Massachusetts General Hospital.</li>
	<li>To offer full PMS2 testing, Invitae has developed a next-generation sequencing (NGS) method for sequencing and deletion/duplication analysis of the full PMS2 gene. This approach has been validated to demonstrate an accuracy, reproducibility, analytical sensitivity and specificity of 100%.</li>
</ul>

<p><strong>Studies</strong></p>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Invitae hereditary cancer validation study</h4>

<div class="expandable-content">
<p><strong>A systematic comparison of traditional and multi-gene panel testing for hereditary breast and ovarian cancer genes in more than 1000 patients</strong></p>

<p><strong>Overview</strong></p>

<p>A study comparing Invitae’s panel test to traditional BRCA1 and BRCA2 tests in more than 1000 patients was undertaken in collaboration with the Stanford University School of Medicine and Massachusetts General Hospital. The study demonstrated 100% analytic sensitivity and specificity for Invitae’s panel compared to traditional genetic test results for both sequence alterations and deletions/duplications. Variant classifications were also highly (99.8%) concordant.</p>

<p><strong>Background</strong></p>

<p>Multi-gene panels for hereditary breast and ovarian cancer risk assessment are gaining acceptance, not only as additions to but also as replacements for traditional BRCA1/2 testing. To help determine which tests are appropriate for any given patient, it is important to understand the analytic and clinical performance of these tests by comparison with traditional testing.</p>

<p><strong>Methods</strong></p>

<p>A total of 1105 individuals were tested using an Invitae 29-gene hereditary cancer panel. Sequence alterations and copy number deletions/duplications were determined by next-generation sequencing (NGS) using Invitae’s custom biochemical and bioinformatics methodologies. For these 1105 individuals, high-quality reference and confirmatory data were available for direct comparison. Variants were classified using a framework (Sherloc) based on the American College of Medical Genetics and Genomics 2015 guidelines using only publicly available and not proprietary data resources. Classifications were compared for 975 individuals for whom traditional BRCA1/2 test results from Myriad Genetics were available.</p>

<p><strong>Table 1: Analytic concordance</strong></p>

<p><img src="https://invitae.com/static/img/public/JMD_2015_Table1.png" alt="Table 1: Analytic concordance" style="width:400px;"></p>

<div style="float:left; margin:0; width:50%;">
<p><strong>Figure 1: Types of pathogenic variants observed</strong></p>

<p><img src="https://invitae.com/static/img/public/JMD_2015_Figure1.png" alt="Figure 1: Types of pathogenic variants observed" style="width:400px;"></p>
</div>

<div style="float:left; margin:0; width:50%;">
<p><strong>Table 2: Interpretation concordance for BRCA1/2</strong></p>

<p><img src="https://invitae.com/static/img/public/JMD_2015_Table2.png" alt="Table 2: Interpretation concordance for BRCA1/2" style="width:350px;"><br>
<br>
<br>
 </p>
</div>

<p><strong>Results</strong></p>

<ul class="bullet-list">
	<li>100% analytic sensitivity and specificity was observed across all 750 comparable variant calls in the 1105 individuals.</li>
	<li>These 750 variants included 48 technically challenging examples of sequence and/or copy number variation that together represented a significant fraction (13.4%) of the pathogenic variants in the prospective cases.</li>
	<li>Considering variant classifications for BRCA1/2, 99.8% report concordance was observed.</li>
	<li>The rates of variants of uncertain significance for BRCA1/2 testing were comparable, albeit slightly higher, in the Invitae test versus the traditional tests (4.1% vs. 3.2%).</li>
	<li>Consistent with other studies of comparable populations, 4.5% of the BRCA1/2-negative patients had a mutation uncovered in another cancer risk gene.</li>
</ul>

<p><strong>Discussion</strong></p>

<p>Invitae’s NGS panel test can provide analytic and clinical results highly comparable to those of traditional BRCA1/2 testing. For both sequence and deletion/duplication variants across many genes, 100% sensitivity and specificity was observed, as well as high interpretation concordance (99.8%). Panel tests can also uncover potentially actionable findings that may be otherwise missed. A detailed study of the clinical actionability of non-BRCA1/2 variants observed in these and other patients is reported separately.</p>

<p><strong>Publication</strong></p>

<p>This study is published in the <i>Journal of Molecular Diagnostics</i>, the official journal of the Association for Molecular Pathology.</p>

<p>Stephen E Lincoln, Yuya Kobayashi, Michael J Anderson, Shan Yang, Andrea J Desmond, Meredith A Mills, Geoffrey B Nilsen, Kevin B Jacobs, Federico A Monzon, Allison W Kurian, James M Ford, Leif W Ellisen, <a target="_blank" href="http://jmd.amjpathol.org/article/S1525-1578(15)00128-2/abstract">A systematic comparison of traditional and multi-gene panel testing for hereditary breast and ovarian cancer genes in more than 1000 patients</a>. <i>J Mol Diagn.</i> 2015, <i>in press.</i></p>

<p>Download the <a target="_blank" href="http://marketing.invitae.com/acton/attachment/7098/f-00a8/1/-/-/-/-/WP101%20WhitePaper%20Analytic%20Validation%20Summary.pdf">Invitae hereditary cancer analytic validation</a> one-page PDF of this information.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Clinical evaluation of multi-gene hereditary cancer panels</h4>

<div class="expandable-content">
<p>To demonstrate the value of multi-gene panels in hereditary cancer risk assessment, Invitae collaborated with Stanford University researchers, James Ford, M.D. and Allison W. Kurian, MD, MSc. The results of this research, recently published in the Journal of Clinical Oncology, show that that multi-gene hereditary cancer panels can offer comparable performance to traditional BRCA1/2 genetic testing and can provide additional clinical benefit to doctors and patients seeking cancer risk assessment.</p>

<p>To learn more about this publication, visit our <a href="{% page_url 'portal_common.clinical' %}"> Clinical Studies</a> page.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Sequencing and deletion/duplication analysis of exons 12-15 of PMS2 using next-generation sequencing (NGS)</h4>

<div class="expandable-content">
<p><strong>Overview</strong></p>

<p>Lynch syndrome, also known as hereditary non-polyposis colorectal cancer (HNPCC), is characterized by familial predisposition to cancers of the colon, endometrium, ovary, stomach, and urinary tract.<sup>1</sup> Most cases of Lynch syndrome are caused by variants in MLH1, MSH2, and MSH6, but 4–11 percent of cases are caused by variants in PMS2.<sup>2-4</sup></p>

<p>Testing for inherited variants in PMS2 is hampered by the presence of a pseudogene, PMS2CL, which has nearly identical homology to PMS2 in the final four exons of the gene (exons 12–15). Thus, sequence reads derived from hybridization capture in next-generation sequencing (NGS) methods cannot be unambiguously aligned to PMS2 or PMS2CL. Gene conversion between exons 12 and 15 of PMS2 and PMS2CL further complicates this issue.<sup>5</sup></p>

<p><strong>Why develop an NGS method?</strong></p>

<p>Invitae is committed to making high-quality genetic testing affordable and accessible. Most laboratories perform multiplex ligation-dependent probe amplification (MLPA) to identify deletion/duplication variants, and use long-range PCR (LR-PCR) before sequencing to identify read-through variants and avoid interference from the PMS2CL pseudogene. This is a highly customized and resource-intensive approach to the analysis of a single gene in every sample. Having developed an approach that maximizes the use of our established workflows and capabilities, we are able to offer sequencing of this difficult but important region of PMS2 while maintaining our commitment to affordability.</p>

<p><strong>Invitae's approach to PMS2</strong></p>

<p>Invitae’s approach to the evaluation of exons 12–15 of PMS2 is a two-step process for read-through variants and a three-step process for deletions and duplications (Figure 1). The first step for both types of variants is a bioinformatics screen in which sequence reads derived from both PMS2 and the paralogous PMS2CL gene are analyzed for the presence of variants using PMS2 as the reference sequence. For read-through variants, non-benign variants identified in the screen are definitively assigned to PMS2 or PMS2CL using Sanger sequencing of LR-PCR products of PMS2 (exons 12–15) and PMS2CL (exons 3–6). For deletion/duplication variants, the second step is to confirm the bioinformatics screen call with MLPA, and to account for the possibility of gene conversion, a final step with LR-PCR is used to disambiguate the location of the variant.<sup>6</sup></p>

<p><img src="http://marketing.invitae.com/cdnr/47/acton/attachment/7098/f-018a/1/-/-/-/-/PMS2_fig1.png" alt="Figure 1. Invitae’s method of PMS2 sequencing and deletion/duplication analysis" style="height:315px;"></p>

<p><strong>Validation Data</strong></p>

<p>This approach was validated with samples known to have specific variants in these exons for both genes (reference set). For validation of the read-through method, we analyzed 32 unique samples carrying 205 true positive and 34,876 true negative variants in PMS2 or PMS2CL and demonstrated an accuracy, reproducibility, and analytical sensitivity and specificity of 100% (Table 1). For validation of the deletion/duplication method, we analyzed 28 unique samples carrying 90 true positive and 50 true negative individual exon variants in PMS2 or PMS2CL and demonstrated an accuracy, reproducibility, and analytical sensitivity and specificity of 100% (Table 2).</p>

<p><img src="http://marketing.invitae.com/cdnr/47/acton/attachment/7098/f-018b/1/-/-/-/-/PMS2_table1_2.png" alt="Table 2 and Table 3. Analytical sensitivity and specificity of PMS2 sequencing and deletion/duplication analysis" style="height:500px;"></p>

<p><strong>References</strong></p>

<p class="endnote">1. Lynch, HT, et al. Review of the Lynch syndrome: history, molecular genetics, screening, differential diagnosis, and medicolegal ramifications. Clinical Genetics. 2009; 76(1):1-18. PMID: 19659756<br>
2. Gill, S, et al. Isolated loss of PMS2 expression in colorectal cancers: frequency, patient age, and familial aggregation. Clinical Cancer Research. 2005; 11:6466-6471. PMID: 16166421<br>
3. Halvarsson, B, et al. The added value of PMS2 immunostaining in the diagnosis of hereditary nonpolyposis colorectal cancer. Familial Cancer. 2006; 5:353-358. PMID: 16817031<br>
4. Truninger, K, et al. Immunohistochemical analysis reveals high frequency of PMS2 defects in colorectal cancer. Gastroenterology. 2005;128:1160-1171. PMID: 15887099<br>
5. Hayward, BE, et al. Extensive gene conversion at the PMS2 DNA mismatch repair locus. Human Mutation. 2007; 28(5):424-30. PMID: 17253626<br>
6. Vaughn CP, et al. Avoidance of pseudogene interference in the detection of 3’ deletions in PMS2. Human Mutation. 2011; 32(9):1063-71. PMID: 21618646</p>

<p>To learn more, please read our <a target="blank" href="http://marketing.invitae.com/acton/attachment/7098/f-0139/1/-/-/-/-/WP103-1_PMS2%20Sequencing%20NGS%20Validation%20Summary.pdf
                            ">PMS2 sequencing and deletion/duplication validation statement</a>.</p>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<p> </p>

<p>We are happy to share more details on any of our validation studies with you. Please <a href="{% page_url 'portal_common.contact' %}">contact</a> Client Services to request additional information.</p>
</div>
</div>
</div>
