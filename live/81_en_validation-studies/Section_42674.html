<div class="row">
<div class="col-md-12">
<h3>Genetic test results you can trust</h3>

<p>To demonstrate that Invitae's next-generation sequencing (NGS) analysis provides the high-quality results you are accustomed to, Invitae has validated our analytic results and clinical interpretations through a number of studies:</p>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Invitae hereditary cancer validation study</h4>

<div class="expandable-content">
<p><strong>A systematic comparison of traditional and multi-gene panel testing for hereditary breast and ovarian cancer genes in more than 1000 patients</strong></p>

<p><strong>Overview</strong></p>

<p>A study comparing Invitae’s panel test to traditional BRCA1 and BRCA2 tests in more than 1000 patients was undertaken in collaboration with the Stanford University School of Medicine and Massachusetts General Hospital. The study demonstrated 100% analytic sensitivity and specificity for Invitae’s panel compared to traditional genetic test results for both sequence alterations and deletions/duplications. Variant classifications were also highly (99.8%) concordant.</p>

<p><strong>Background</strong></p>

<p>Multi-gene panels for hereditary breast and ovarian cancer risk assessment are gaining acceptance, not only as additions to but also as replacements for traditional BRCA1/2 testing. To help determine which tests are appropriate for any given patient, it is important to understand the analytic and clinical performance of these tests by comparison with traditional testing.</p>

<p><strong>Methods</strong></p>

<p>A total of 1105 individuals were tested using an Invitae 29-gene hereditary cancer panel. Sequence alterations and copy number deletions/duplications were determined by next-generation sequencing (NGS) using Invitae’s custom biochemical and bioinformatics methodologies. For these 1105 individuals, high-quality reference and confirmatory data were available for direct comparison. Variants were classified using a framework (Sherloc) based on the American College of Medical Genetics and Genomics 2015 guidelines using only publicly available and not proprietary data resources. Classifications were compared for 975 individuals for whom traditional BRCA1/2 test results from Myriad Genetics were available.</p>

<p><strong>Table 1: Analytic concordance</strong></p>

<p><img src="https://invitae.com/static/img/public/JMD_2015_Table1.png" style="width:400px;" alt="Table 1: Analytic concordance"></p>

<div style="float:left; margin:0; width:50%;">
<p><strong>Figure 1: Types of pathogenic variants observed</strong></p>

<p><img src="https://invitae.com/static/img/public/JMD_2015_Figure1.png" style="width:400px;" alt="Figure 1: Types of pathogenic variants observed"></p>
</div>

<div style="float:left; margin:0; width:50%;">
<p><strong>Table 2: Interpretation concordance for BRCA1/2</strong></p>

<p><img src="https://invitae.com/static/img/public/JMD_2015_Table2.png" style="width:350px;" alt="Table 2: Interpretation concordance for BRCA1/2"><br>
<br>
<br>
 </p>
</div>

<p><strong>Results</strong></p>

<ul class="bullet-list">
	<li>100% analytic sensitivity and specificity was observed across all 750 comparable variant calls in the 1105 individuals.</li>
	<li>These 750 variants included 48 technically challenging examples of sequence and/or copy number variation that together represented a significant fraction (13.4%) of the pathogenic variants in the prospective cases.</li>
	<li>Considering variant classifications for BRCA1/2, 99.8% report concordance was observed.</li>
	<li>The rates of variants of uncertain significance for BRCA1/2 testing were comparable, albeit slightly higher, in the Invitae test versus the traditional tests (4.1% vs. 3.2%).</li>
	<li>Consistent with other studies of comparable populations, 4.5% of the BRCA1/2-negative patients had a mutation uncovered in another cancer risk gene.</li>
</ul>

<p><strong>Discussion</strong></p>

<p>Invitae’s NGS panel test can provide analytic and clinical results highly comparable to those of traditional BRCA1/2 testing. For both sequence and deletion/duplication variants across many genes, 100% sensitivity and specificity was observed, as well as high interpretation concordance (99.8%). Panel tests can also uncover potentially actionable findings that may be otherwise missed. A detailed study of the clinical actionability of non-BRCA1/2 variants observed in these and other patients is reported separately.</p>

<p><strong>Publication</strong></p>

<p>This study is published in the <i>Journal of Molecular Diagnostics</i>, the official journal of the Association for Molecular Pathology.</p>

<p>Stephen E Lincoln, Yuya Kobayashi, Michael J Anderson, Shan Yang, Andrea J Desmond, Meredith A Mills, Geoffrey B Nilsen, Kevin B Jacobs, Federico A Monzon, Allison W Kurian, James M Ford, Leif W Ellisen, <a target="_blank" href="http://jmd.amjpathol.org/article/S1525-1578(15)00128-2/abstract">A systematic comparison of traditional and multi-gene panel testing for hereditary breast and ovarian cancer genes in more than 1000 patients</a>. <i>J Mol Diagn.</i> 2015<i>.</i></p>

<p>Download the <a target="_blank" href="http://marketing.invitae.com/acton/attachment/7098/f-00a8/1/-/-/-/-/WP101%20WhitePaper%20Analytic%20Validation%20Summary.pdf">Invitae hereditary cancer analytic validation</a> one-page PDF of this information.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Invitae confirmation for clinical genetic testing</h4>

<div class="expandable-content">
<p>Next-generation sequencing (NGS) has largely replaced Sanger sequencing, an older technology, in clinical genetic tests. Compared to Sanger, NGS provides lower costs, higher throughput, and the ability to easily test multiple clinically relevant genes in each patient. In order to minimize the risk of false positives from NGS, a two-step approach is often used, whereby variants uncovered by NGS are confirmed by a separate assay (such as Sanger sequencing). Such confirmatory testing must be “orthogonal” to NGS: it needs to employ different biochemical operating principles and have an uncorrelated chance of error.</p>

<p>Confirmatory testing adds cost, manual labor, and time to the genetic testing process. The ACMG guidelines for NGS state that laboratories should have “extensive experience with NGS… before deciding that result confirmation with orthogonal technology can be eliminated.”<sup>1</sup> It has been reported that confirmation of the highest quality NGS variant calls may be unnecessary.<sup>2–5</sup> Moreover, naive use of confirmatory testing can in fact introduce more errors than it actually prevents.<sup>2</sup></p>

<p><strong>Confirmation is unnecessary and wasteful for high-confidence NGS variant calls</strong></p>

<p>In collaboration with the Partners Laboratory for Molecular Medicine at Harvard and the National Institute of Standards and Technology (NIST), Invitae recently completed the largest study to date on the question of whether and when orthogonal confirmation of NGS results is required.<sup>6</sup> By using both clinical samples (n = 80,000) as well as gold-standard reference samples from NIST, our study considered almost 200,000 variant calls with confirmatory data.</p>

<p>The results reaffirmed other, previous studies in demonstrating that not all variants require confirmation. We showed that high-confidence NGS variant calls can be identified using objective data quality metrics,<sup>6</sup> and that this high-confidence population contains <strong>no</strong> false positives: 100% of the high-confidence variant calls were proven correct by orthogonal data. Many variants meet this “high confidence” criteria and thus do not benefit from confirmation (i.e., confirmation cannot further improve the accuracy of these calls). The remaining, lower confidence calls include a mixture of true and false positives: these cases require, and are resolved by, confirmatory testing.</p>

<p><strong>A significant improvement over others’ approaches</strong></p>

<p>The key question is how to consistently identify which NGS calls require confirmation. In this aspect, our study differs from prior publications. Our analysis shows that a battery of quality metrics (based on recommendations in the AMP/CAP NGS bioinformatics guidelines<sup>7</sup>) is required to catch 100% of false positives.<sup>6</sup> Prior studies by other laboratories used only one or two metrics, such as quality score or read depth. We find that these simpler criteria miss some false positives, potentially allowing incorrect pathogenic variants to escape confirmation and be reported as real. We attribute this difference to the size of our study, which was 100 to 1,000 times larger than previous studies, permitting the development of more effective criteria. Our study also employed statistical confidence measures, a critical step that most prior studies did not perform.</p>

<p><b>False positive rate and sensitivity in variant calling</b></p>

<p>There is always a trade-off between sensitivity (the ability to detect variants that are real) and specificity (the ability to avoid false positives). In order to identify clinically important variants with high sensitivity, a wide net must be cast. However, in doing so, a population of lower confidence calls is also identified, some of which are true and some false. Any test that tries to eliminate confirmation by using very strict calling (aiming for high specificity without confirmation) will suffer a sensitivity penalty: true positives will be missed by such a test. Confirmation of some NGS calls continues to be a necessary component of sensitive genetic tests.</p>

<table cellspacing="1" style="width:500px;" border="1" cellpadding="1" class="table-with-lines">
	<thead>
		<tr bgcolor="#EFEDEA">
			<th scope="col">Category</th>
			<th scope="col">Call quality</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		<tr bgcolor="#A3CF71">
			<td><strong>High-confidence true positives</strong></td>
			<td><strong>Meets highly stringent criteria</strong></td>
			<td><strong>No benefit from confirmation</strong></td>
		</tr>
		<tr bgcolor="#F2B857">
			<td><strong>Candidate true positives</strong></td>
			<td><strong>Does not meet stringent criteria</strong></td>
			<td><strong>Require confirmation</strong></td>
		</tr>
	</tbody>
</table>

<p><i>NGS variants that pass filtering can be placed into high-confidence and intermediate-confidence categories.<sup>6</sup></i></p>

<p><strong>Confirmation methods</strong></p>

<p>Variant calls that require confirmation are of many different types, necessitating the use of multiple different confirmation methods. In addition to Sanger sequencing, array CGH, and MLPA, Invitae validated the Pacific Biosciences platform (PacBio) as a confirmation method, showing 100% concordance between PacBio and Sanger.<sup>8</sup> PacBio’s technology is highly orthogonal to NGS and can test variants that are difficult for Sanger.<sup>9</sup> Compared to Sanger sequencing, PacBio also provides higher throughput, a higher assay success rate, and improved quality control.<sup>8</sup> By having multiple platforms available, Invitae can use the most appropriate method for each clinical case.</p>

<p><strong>Conclusion</strong></p>

<p>Confirmation significantly increases both cost and turnaround time for patients and clinicians making important healthcare decisions. Our large, interlaboratory study demonstrates that confirmation assays can be focused on a carefully selected subset of variants to deliver high test sensitivity and specificity.</p>

<p>Our team understands that the stakes for clinical genetic testing are high. Results can lead to irreversible action and emotional distress for patients and their families. We are committed to maintaining the highest quality, while continually improving our processes in a responsible and data-driven manner. We hope this study will inform a new standard of data-driven best practices for variant confirmation.</p>

<p><strong>References</strong></p>

<ol class="numbered-list">
	<li>Rehm HL, Bale SJ, Bayrak-Toydemir P <i>et al</i>. <i>Genet Med</i>. 2013;15(9):733-47.</li>
	<li>Beck TF, Mullikin JC; NISC Comparative Sequencing Program, Biesecker LG. <i>Clin Chem</i>. 2016;62(4):647-54.</li>
	<li>Baudhuin LM, Lagerstedt SA, Klee EW <i>et al</i>. <i>J Mol Diagn</i>. 2015;17(4):456-61.</li>
	<li>Mu W, Lu HM, Chen J <i>et al</i>. <i>J Mol Diagn</i>. 2016;18(6):923-32.</li>
	<li>Strom SP, Lee H, Das K <i>et al</i>. <i>Genet Med</i>. 2014;16(7):510-5.</li>
	<li>Lincoln SE, Truty R, Lin CF <i>et al. BioRxiv.</i> 2018, May 18. [Epub ahead of print]</li>
	<li>Roy S, Coldren C, Karunamurthy A <i>et al</i>. <i>J Mol Diagn</i>. 2018;20(1):4-27.</li>
	<li>McCalmon S, Konvicka K, Reddy NR <i>et al. </i>Poster presented at: The American Society of Human Genetics Annual Meeting; 2016, October 18-22; Vancouver, Canada.</li>
	<li>Chaisson MJ, Huddleston J, Dennis MY <i>et al. Nature.</i> 2015;517:608-611.</li>
</ol>

<p class="endnote"><i></i></p>

<p>Download the <a target="_blank" href="https://marketing.invitae.com/acton/attachment/7098/f-04b5/1/-/-/-/-/WP107_InvitaeConfirmationClinicalGeneticTesting.pdf">Invitae confirmation for clinical genetic testing</a> PDF of this white paper.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Clinical evaluation of multi-gene hereditary cancer panels</h4>

<div class="expandable-content">
<p>To demonstrate the value of multi-gene panels in hereditary cancer risk assessment, Invitae collaborated with Stanford University researchers James Ford, M.D. and Allison W. Kurian, MD, MSc. The results of this research, published in the <i>Journal of Clinical Oncology</i>, show that that multi-gene hereditary cancer panels can offer comparable performance to traditional BRCA1/2 genetic testing and can provide additional clinical benefit to doctors and patients seeking cancer risk assessment.</p>

<p>To learn more about this publication, visit our <a href="{% page_url 'portal_common.clinical' %}"> Clinical Actionability</a> page.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Sequencing and deletion/duplication analysis of exons 12-15 of PMS2 using next-generation sequencing (NGS)</h4>

<div class="expandable-content">
<p><strong>Overview</strong></p>

<p>Lynch syndrome, also known as hereditary non-polyposis colorectal cancer (HNPCC), is characterized by familial predisposition to cancers of the colon, endometrium, ovary, stomach, and urinary tract.<sup>1</sup> Most cases of Lynch syndrome are caused by variants in MLH1, MSH2, and MSH6, but 4–11 percent of cases are caused by variants in PMS2.<sup>2-4</sup></p>

<p>Testing for inherited variants in PMS2 is hampered by the presence of a pseudogene, PMS2CL, which has nearly identical homology to PMS2 in the final four exons of the gene (exons 12–15). Thus, sequence reads derived from hybridization capture in next-generation sequencing (NGS) methods cannot be unambiguously aligned to PMS2 or PMS2CL. Gene conversion between exons 12 and 15 of PMS2 and PMS2CL further complicates this issue.<sup>5</sup></p>

<p><strong>Why develop an NGS method?</strong></p>

<p>Invitae is committed to making high-quality genetic testing affordable and accessible. Most laboratories perform multiplex ligation-dependent probe amplification (MLPA) to identify deletion/duplication variants, and use long-range PCR (LR-PCR) before sequencing to identify read-through variants and avoid interference from the PMS2CL pseudogene. This is a highly customized and resource-intensive approach to the analysis of a single gene in every sample. Having developed an approach that maximizes the use of our established workflows and capabilities, we are able to offer sequencing of this difficult but important region of PMS2 while maintaining our commitment to affordability.</p>

<p><strong>Invitae's approach to PMS2</strong></p>

<p>Invitae’s approach to the evaluation of exons 12–15 of PMS2 is a two-step process for read-through variants and a three-step process for deletions and duplications (Figure 1). The first step for both types of variants is a bioinformatics screen in which sequence reads derived from both PMS2 and the paralogous PMS2CL gene are analyzed for the presence of variants using PMS2 as the reference sequence. For read-through variants, non-benign variants identified in the screen are definitively assigned to PMS2 or PMS2CL using Sanger sequencing of LR-PCR products of PMS2 (exons 12–15) and PMS2CL (exons 3–6). For deletion/duplication variants, the second step is to confirm the bioinformatics screen call with MLPA, and to account for the possibility of gene conversion, a final step with LR-PCR is used to disambiguate the location of the variant.<sup>6</sup></p>

<p><img src="http://marketing.invitae.com/cdnr/47/acton/attachment/7098/f-018a/1/-/-/-/-/PMS2_fig1.png" style="height:315px;" alt="Figure 1. Invitae’s method of PMS2 sequencing and deletion/duplication analysis"></p>

<p><strong>Validation Data</strong></p>

<p>This approach was validated with samples known to have specific variants in these exons for both genes (reference set). For validation of the read-through method, we analyzed 32 unique samples carrying 205 true positive and 34,876 true negative variants in PMS2 or PMS2CL and demonstrated an accuracy, reproducibility, and analytical sensitivity and specificity of 100% (Table 1). For validation of the deletion/duplication method, we analyzed 28 unique samples carrying 90 true positive and 50 true negative individual exon variants in PMS2 or PMS2CL and demonstrated an accuracy, reproducibility, and analytical sensitivity and specificity of 100% (Table 2).</p>

<p><img src="http://marketing.invitae.com/cdnr/47/acton/attachment/7098/f-018b/1/-/-/-/-/PMS2_table1_2.png" style="height:500px;" alt="Table 2 and Table 3. Analytical sensitivity and specificity of PMS2 sequencing and deletion/duplication analysis"></p>

<p><strong>References</strong></p>

<p class="endnote">1. Lynch, HT, et al. Review of the Lynch syndrome: history, molecular genetics, screening, differential diagnosis, and medicolegal ramifications. Clinical Genetics. 2009; 76(1):1-18. PMID: 19659756<br>
2. Gill, S, et al. Isolated loss of PMS2 expression in colorectal cancers: frequency, patient age, and familial aggregation. Clinical Cancer Research. 2005; 11:6466-6471. PMID: 16166421<br>
3. Halvarsson, B, et al. The added value of PMS2 immunostaining in the diagnosis of hereditary nonpolyposis colorectal cancer. Familial Cancer. 2006; 5:353-358. PMID: 16817031<br>
4. Truninger, K, et al. Immunohistochemical analysis reveals high frequency of PMS2 defects in colorectal cancer. Gastroenterology. 2005;128:1160-1171. PMID: 15887099<br>
5. Hayward, BE, et al. Extensive gene conversion at the PMS2 DNA mismatch repair locus. Human Mutation. 2007; 28(5):424-30. PMID: 17253626<br>
6. Vaughn CP, et al. Avoidance of pseudogene interference in the detection of 3’ deletions in PMS2. Human Mutation. 2011; 32(9):1063-71. PMID: 21618646</p>

<p>To learn more, please read our <a target="blank" href="http://marketing.invitae.com/acton/attachment/7098/f-0139/1/-/-/-/-/WP103-1_PMS2%20Sequencing%20NGS%20Validation%20Summary.pdf
                            ">PMS2 sequencing and deletion/duplication validation statement</a>.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Invitae’s approach to testing SMN1 and SMN2 for spinal muscular atrophy</h4>

<div class="expandable-content">
<p>Complete loss of SMN1 gene function results in spinal muscular atrophy (SMA), an early-onset debilitating neuromuscular disorder characterized by loss of motor neurons in the spinal cord. SMN1 has a near-identical gene copy named SMN2 also located on chromosome 5, approximately 800 kilobases from SMN1. The coding regions of SMN2 and SMN1 differ from one another by a single nucleotide in exon 7*, which we term the gene-determining variant (GDV). This difference adversely affects splicing of the exon and leads to very little full length protein production from the SMN2 gene.</p>

<p>The majority of pathogenic changes in SMA are deletions of SMN1 or gene conversion of SMN1 to SMN2. In addition, rare inactivating sequence variants can occur in SMN1. About 95%–98% of individuals with SMA have zero copies of SMN1 and about 2%–5% are compound heterozygotes, with a deletion of SMN1 on one chromosome and a pathogenic sequence variant in SMN1 on the other chromosome. Notably, the number of SMN2 copies is highly variable among individuals. This number influences the SMA phenotype in patients with SMN1 loss, with severity decreasing and age of onset increasing as the number of SMN2 copies increases.<sup>1,2 </sup></p>

<p><strong>Challenges in SMA testing and Invitae's NGS-based approach</strong></p>

<p>Most laboratories traditionally diagnose SMA by performing multiplex ligation-dependent probe amplification (MLPA) or quantitative PCR (qPCR) to identify loss of SMN1 exon 7*. These approaches have significant technical limitations and are difficult to efficiently integrate into broader testing.</p>

<p>To address these limitations we developed a comprehensive next-generation sequencing (NGS)-based approach with a customized bioinformatics solution to offer simultaneous sequencing and copy number analysis of these difficult genes while maintaining our commitment to quality and affordability.</p>

<p><img width="90%" alt="Table 1" src="https://marketing.invitae.com/cdnr/47/acton/attachment/7098/f-0614/1/-/-/-/-/WP_SMN1-2_Table1.png"></p>

<p><strong>NGS-based methodology</strong></p>

<p>Invitae has developed a sophisticated assay and bioinformatics solution to accurately detect pathogenic changes in SMN1 and determine SMN2 copy number. First, we align sequencing reads derived from both SMN1 and SMN2 to an SMN1 reference sequence. We then measure total SMN1 + SMN2 copy number using a modified version of CNVitae, our custom-built copy number variant detection algorithm that utilizes NGS read counts. Once we have the total SMN1/2 copy number, individual SMN1 and SMN2 exon 7* copy numbers are determined using the exon 7* GDV. This simultaneous determination of SMN1 and SMN2 exon 7* copy numbers enables high confidence calls for both SMN1 and SMN2** (Figure 1).</p>

<p>We also use the exon 7* GDV to unambiguously place sequence variants in exon 7* of SMN1 and SMN2. The remaining exons (1–6) of SMN1 and SMN2 are identical in sequence, and therefore while we can accurately identify sequence and copy number variants in these exons, their true location within SMN1 or SMN2 cannot be determined. Even though disambiguation is not possible for variants in exons 1–6, their identification can inform the diagnosis of rare compound heterozygous affected individuals.</p>

<p>SMN1/2 exon 7* copy number variants are confirmed by ligation-dependent sequencing, an Invitae innovation that transforms traditional MLPA into a highly scalable NGS method. Sequence variants in exon 7* are confirmed using single-molecule PacBio sequencing, which enables the phasing of the variant with the GDV to unambiguously place the variant in either SMN1 or SMN2.</p>

<table cellspacing="0" style="width: 100%;" border="0" cellpadding="0" class="table-with-lines">
	<tbody>
		<tr>
			<td width="40%">
			<p><strong>Figure 1: SMN1/2 bioinformatics method</strong><br>
			<br>
			Reads derived from both SMN1 and SMN2 are aligned to SMN1, and combined SMN1/2 copy number is determined using Invitae’s read count-based copy number variant detection algorithm, CNVitae. SMN1- and SMN2-specific exon 7* copy number is resolved by counting reads with the gene determining variant in exon 7*.</p>
			</td>
			<td>
			<p><img src="https://marketing.invitae.com/cdnr/47/acton/attachment/7098/f-0612/1/-/-/-/-/SMN1-2_WP_Fig1.png" style="width:300px;" alt="Figure 1: SMN1/2 bioinformatics method"></p>
			</td>
		</tr>
	</tbody>
</table>

<p><br>
<strong>Validation</strong></p>

<p>Our SMN1/2 approach was validated on a set of nine samples available from an external commercial repository of biological samples. SMN1 exon 7* copy number information was previously determined through traditional methods, and SMN2 copy number was known for a subset of these samples.<sup>3</sup> Our method showed 100% sensitivity and specificity for SMN1 and SMN2 copy number, and notably its higher resolution for determining SMN2 copy number enabled us to obtain accurate results for three samples for which copy number had been imprecisely determined with traditional methods previously.<sup>3</sup></p>

<p><strong>SMN1 and SMN2 population frequency</strong></p>

<p><img width="80%" alt="Table 2" src="https://marketing.invitae.com/cdnr/47/acton/attachment/7098/f-0613/1/-/-/-/-/WP_SMN1-2_Table2.png"></p>

<p><strong>Footnotes</strong></p>

<p class="endnote">*Reference sequence NM_000344.3, which is used to describe SMN1 sequence variants, contains 8 protein-coding exons. Due to historical reasons, the second and third exons are conventionally referred to as exons 2a and 2b, and the subsequent exons are referred to as exons 3–7 (PMID: 8838816). At Invitae, systematic exon numbering is used for all genes, including SMN1 and SMN2. For this reason, the gene-differentiating exon conventionally referred to as exon 7 in the literature and in this whitepaper is referred to as exon 8 in our clinical reports.<br>
**Copy number of SMN2 exon 7* is expected to represent copy number for the entire SMN2 gene, and will only be reported for individuals with a positive result in SMN1. CNVs limited to exons 1–6 of SMN1 or SMN2 will not be reported. This assay cannot detect silent carriers (individuals that have 2 functional copies of SMN1 on one chromosome and zero copies on the other). Therefore a negative result for carrier testing greatly reduces but does not eliminate the chance that a person is a carrier.</p>

<p><strong>References</strong></p>

<p class="endnote">1. Mailman MD <i>et al. </i>Molecular analysis of spinal muscular atrophy and modification of the phenotype by SMN2. <i>Genet. Med.</i> 2002;4:20–6. PMID: 11839954<br>
2. Swoboda KJ <i>et al. </i>Natural history of denervation in SMA: relation to age, SMN2 copy number, and function. <i>Ann Neurol. </i> 2005;57:704– 12. PMID: 15852397<br>
3. Stabley DL <i>et al. </i>SMN1 and SMN2 copy numbers in cell lines derived from patients with spinal muscular atrophy as measured by array digital PCR. <i>Molecular Genetics &amp; Genomic Medicine</i> 2015;3(4):248- 257. PMID: 26247043<br>
4. Hendrickson BC <i>et al.</i> Di erences in SMN1 allele frequencies among ethnic groups within North America. <i>Journal of Medical Genetics </i>2009;46:641-644. PMID: 19625283</p>

<p>Download the <a target="_blank" href="https://marketing.invitae.com/acton/attachment/7098/f-05d2/1/-/-/-/-/WP108_Invitae_WhitePaper_SMN1-SMN2.pdf">one-page PDF</a> of this white paper, which includes an appendix not shown here.</p>
</div>
</div>

<div class="expandable-item">
<h4><span class="icon icon-angle-right"></span>Invitae PGT-A can accurately detect whole-chromosome aneuploidy, segmental aneuploidy, haploidy, polyploidy, and uniparental isodisomy</h4>

<div class="expandable-content">
<p><strong>Overview</strong></p>

<p>Invitae’s preimplantation genetic testing for aneuploidy (PGT-A) is an NGS-based assay that uses proprietary technology (FAST-SeqS) that allows for robust amplification and deep sequencing (~1 million reads) of over 20,000 regions (Line1 sites) across the genome to call whole-chromosome and segmental aneuploidy. Additionally, Invitae PGT assesses data from over 10,000 single-nucleotide polymorphic (SNP) sites across the genome to call haploidy, all forms of triploidy, other forms of polyploidy, in addition to many instances of uniparental isodisomy (UPiD).</p>

<p><strong>Superior detection:</strong> Invitae PGT can accurately detect a wide-spectrum of abnormalities, including whole-chromosome aneuploidy, segmental aneuploidy (≥10 MB), polyploidy, and UPiD.<sup>1,2,3</sup></p>

<p><strong>Comprehensive coverage:</strong> Unlike most NGS-based PGT assays (which use whole-genome amplification (WGA)), Invitae PGT’s deep sequencing approach captures SNP information, allowing for the detection of haploidy, polyploidy, and UPiD for select chromosomes, abnormalities that are associated with poor reproductive outcomes and are incompletely detected by other NGS-based PGT technologies (Figures 1 and 2). Recent validation studies have confirmed that Invitae’s new PGT laboratory, located in San Francisco, California, is able to accurately detect whole-chromosome and segmental aneuploidy, polyploidy, and UPiD. The results of this validation are evidence of this assay’s reproducibility and robustness, as similar accuracy was reported from the former lab location in Cambridge, Massachusetts. This paper summarizes these validation experiments and results.</p>

<p><strong>Background</strong></p>

<p>Identifying embryos with the greatest chance of implantation and live birth is vital to improving IVF success rates.</p>

<p>To date, all validation studies aimed at assessing Invitae PGT’s capabilities have been performed in the Cambridge, Massachusetts, laboratory. Invitae has recently built a new state-of-the-art PGT laboratory in San Francisco, California. Prior to accepting patient samples, a series of validation experiments were performed to confirm Invitae’s PGT assay performance in its new laboratory.</p>

<p><strong>Methods</strong></p>

<p>Samples from whole chromosome aneuploid (n=6), segmental aneuploid (n=121), triploid (n=5), UPiD (n=3), and known diploid cell lines (n=8, including both euploid and aneuploid samples) were run in replicate, and the resulting data were processed with the validated algorithms in the new San Francisco PGT laboratory. Sample calls were compared to the expected karyotypes to estimate analytical sensitivity and specificity for detection of whole- chromosome aneuploidy, segmental aneuploidy, polyploidy, and UPiD.</p>

<p><img width="90%" alt="Figure 1: PGT-A amplification strategies and figure 2: Result examples using both count-based and SNP-based data" src="https://marketing.invitae.com/cdnr/47/acton/attachment/7098/f-0c48/1/-/-/-/-/WhitePaper_PGT_Figures1-2.png"></p>

<p><strong>Results</strong></p>

<ul class="bullet-list">
	<li>Sensitivity and specificity for detection of whole-chromosome aneuploidy was 100% (95% confidence interval [CI] 82.4–100% and 77.2–100% for sensitivity and specificity, respectively)</li>
	<li>Sensitivity and specificity for detection of segmental aneuploidy ≥10 Mb was 97.7% and 100%, respectively (95% CI 94.1–99.4% and 75.3–100% for sensitivity and specificity, respectively)</li>
	<li>Sensitivity and specificity for detection of triploidy was 100% (95% CI 77.2–100% and 92.0–100% for sensitivity and specificity, respectively)</li>
	<li>Sensitivity and specificity for detection of UPiD was 100% (95% CI 80.6–100% and 92.0–100% for sensitivity and specificity, respectively)</li>
</ul>

<p><strong>Discussion</strong></p>

<p>Launching an existing assay in a new location requires extensive validation, even if the technology is not changing. As expected, our assay performs similarly in both locations offering a high accuracy for the detection of euploid embryos. Invitae is now accepting patient PGT samples in our San Francisco laboratory. Reporting on haploidy, polyploidy, and UPiD in addition to whole-chromosome and segmental aneuploidy is essential to decreasing miscarriage rates in PGT-derived pregnancies (Figure 3).</p>

<p><strong>Figure 3: Invitae PGT can detect the most frequent causes of miscarriage due to chromosome abnormalities</strong></p>

<p><img width="60%" alt="Figure 3: Invitae PGT can detect the most frequent causes of miscarriage due to chromosome abnormalities" src="https://marketing.invitae.com/cdnr/47/acton/attachment/7098/f-0c46/1/-/-/-/-/WhitePaper_PGT_Figure3.png"></p>

<p class="endnote">1. Gole J et al. Fertil Steril. 2016;105(2):e25<br>
2. Umbarger MA et al. Fertil Steril. 2016;106(3):e152.<br>
3. Umbarger MA et al. Fertil Steril 2017;108(3):e270.<br>
4. Levy B et al. Obstet Gynecol. 2014;124(2 Pt 1):202-9.</p>

<p>Download the <a target="_blank" href="https://marketing.invitae.com/acton/attachment/7098/f-0a37/1/-/-/-/-/WP109_Invitae_Whitepaper_PGT_Validation.pdf">PDF</a> of this white paper.</p>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<p></p>

<p>We are happy to share more details on any of our validation studies with you. Please <a href="{% page_url 'portal_common.contact' %}">contact</a> Client Services to request additional information.</p>
</div>
</div>
</div>